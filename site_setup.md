## Site setup

Create a "Sites" folder to contain all client projects. This folder will act as "localhost" for the server configuration.

Open terminal and run the following command, `mkdir ~/Sites && cd ~/Sites`

Create a folder for the clients project, `mkdir [client folder]`

## Apache

Modify the `httpd.conf` file to get Apace up and running. Before anything, **make a duplicate** of the file, `sudo cp /etc/apache2/httpd.conf /etc/apache2/httpd-bk.conf`.

Uncomment lines 166 - 169.

* `LoadModule alias_module libexec/apache2/mod_alias.so`
* `LoadModule rewrite_module libexec/apache2/mod_rewrite.so`
* `LoadModule php5_module libexec/apache2/libphp5.so`

Modify `/etc/apache2/extra/httpd-userdir.conf` and uncomment line 16, `Include /private/etc/apache2/users/*.conf`.


## MySQL



## PHP



## Drupal



## Wordpress