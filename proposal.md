##Web and Mobile Design System

##Description**

What is a Design System? Well, a design system is "everything that makes up a product". Everything? Everything. From typography, layouts and grids, colors, icons, components and coding conventions, to voice and tone, style-guide and documentation, a design system is bringing all of these together in a way that allows an entire team to learn, build, and grow.

Why should we start building something custom over beginning with an existing framework like Bootstrap or Foundation? These are a few key factors in deciding against using the latter:

  * A client might already have a custom design. Visual designs for a new or existing product might already be complete. If a framework is used, a significant amount of customization will be needed to make it fit the designs.
  * Establishing coding conventions, naming conventions, etc. based on a team's preferences. Again, a ton of time re-naming classes or reorganizing things to fit the team's needs would be required.

The amount of time spent essentially gutting a framework does not seem worth it. Building our own framework would be beneficial to us in the long run, more maintainable, and, as a bonus, would be an incredible team experience.

##Establishing top level goals

  * **Organization**: A messy codebase can become a nightmare to work with. Making sure there is a well thought-out structure and approach is very important.
  * **Maintainability**: Over time, there are going to be new developers/designers/architects jumping in to fix bugs and add features. Having proper guidelines and conventions make it easier for people to do things correctly.
  * **Agnostic**: With an ever increasing rise in mobile/tablet traffic, it is very important to make the experience and codebase platform-agnostic.
  * **Scalability**: As clients grow, so do their products. Creating a system that will grow in paralell is not only essential for a clients future, it is equally important for design and development teams to promote future ideas and innovations.

A majority of today's web, mobile, or hybrid application projects do not take a holistic approach to strategy, design, development, performance, scalability, and/or maintainability.

The following are a few key facets of a centralized design system:

* customers will have the ability to plug into as little or as much of the system as needed to support their brand and effort.
* customers will be delivered a system that has scalability into future projects with minimal effort or refactoring.
* designers and developers will have a centralized system that greatly increases development time and reduces time to market.
